# GameOfLife

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/e295d4d98f9743d1849ad674c412b197)](https://www.codacy.com/app/petoknm/GameOfLife?utm_source=github.com&utm_medium=referral&utm_content=petoknm/GameOfLife&utm_campaign=badger)

A simple C++ terminal Conway's Game Of Life implementation

## Dependencies
 * ncurses

## Compilation
To compile it you need these two commands:
 * "cmake ."
 * "make"
 
## Run
To run it just execute the output with no parameters
 * "./GameOfLife"
