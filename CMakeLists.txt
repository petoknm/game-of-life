cmake_minimum_required(VERSION 3.4)
project(GameOfLife)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp)
add_executable(GameOfLife ${SOURCE_FILES})

find_package(Curses REQUIRED)
target_link_libraries(GameOfLife ncurses)