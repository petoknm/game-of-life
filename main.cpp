#include <iostream>
#include <vector>
#include <ncurses.h>

using namespace std;

typedef vector<vector<bool>> Field;

void printField(const Field &field) {

    move(0, 0);
    for (const auto &row : field) {
        for (const auto &val : row) {
            addch(val ? '\xdb' : ' ');
        }
    }
    refresh();
}

void resizeField(Field &field, unsigned width, unsigned height) {

    field.resize(height);
    for (auto &row : field) {
        row.resize(width);
    }
}

const bool readField(const Field &field, int x, int y) {

    unsigned long width = field[0].size();
    unsigned long height = field.size();

    while (x < 0) x += width;
    while (x >= width) x -= width;

    while (y < 0) y += height;
    while (y >= height) y -= height;

    return field[y][x];
}

inline void setField(Field &field, unsigned int x, unsigned int y, bool value) {
    field[y][x] = value;
}

unsigned int countNeighbors(const Field &field, int x, int y) {

    unsigned int count = 0;

    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (!(dx == 0 && dy == 0) && readField(field, x + dx, y + dy)) {
                count++;
            }
        }
    }

    return count;
}

void tick(Field &field) {

    Field oldField = field;

    for (unsigned int y = 0; y < field.size(); y++) {
        for (unsigned int x = 0; x < oldField[y].size(); x++) {
            unsigned int neighbors = countNeighbors(oldField, x, y);
            if (oldField[y][x]) {
                // LIVE CELL
                if (neighbors == 2 || neighbors == 3) {
                    // CONTINUE LIVING
                } else {
                    // DIE
                    setField(field, x, y, false);
                }
            } else {
                // DEAD CELL
                if (neighbors == 3) {
                    // COME ALIVE
                    setField(field, x, y, true);
                }
            }
        }
    }
}

void initializeField(Field &field) {

    srand((unsigned int) time(NULL));
    for (auto &row:field) {
        for (auto &&val : row) {
            val = rand() % 2 == 0;
        }
    }
}

int main() {

    initscr();
    curs_set(0);

    unsigned int width, height;
    getmaxyx(stdscr, height, width);

    Field gameField;
    resizeField(gameField, width, height);
    initializeField(gameField);

    for (int i = 0; i < 10e6; i++) {
        printField(gameField);
        tick(gameField);
    }

    getch();
    endwin();

    return 0;
}